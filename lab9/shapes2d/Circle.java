package shapes2d;



public class Circle {
protected int radius;

public Circle(int radius) {
	this.radius=radius;
	
}

public double area() {
	
	double result= Math.PI * radius * radius;
	return result;
}

public String toString() {
	
	return "radius= "+ radius;
}
}
