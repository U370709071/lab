package lab;

public class Circle 
{
	int radius;
	Point center;
	
	public Circle(int r, Point c)
	{
		radius = r;
		center = c;
	}
	
	public double Area()
	{
		return Math.PI*radius * radius;
	}
	
	public double Perimeter()
	{
		return 2*Math.PI*radius;
	}
	
	public boolean Intersect(Circle c)
	{
		double distance = Math.sqrt(Math.pow(center.xCoord - c.center.xCoord,2) +
				Math.pow(center.yCoord - c.center.yCoord,2));
		return distance < c.radius + radius;
		
	}
}
